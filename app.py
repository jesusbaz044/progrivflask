from flask import Flask, render_template, request, redirect, url_for, flash
from flask_mysqldb import MySQL

app = Flask(__name__)
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = 'password'
app.config['MYSQL_DB'] = 'citas'

mysql = MySQL(app)

app.secret_key= 'mysecretkey'

@app.route('/')
def Index():
    cur = mysql.connection.cursor()
    cur.execute('SELECT * FROM consulta')
    data = cur.fetchall()
    return render_template('index.html', consulta = data)

@app.route('/add_consulta', methods=['POST'])
def add_consulta():
    if request.method == 'POST':
        nombre = request.form['nombre']
        hora = request.form['hora']
        cur = mysql.connection.cursor()
        cur.execute('INSERT INTO consulta (nombre, hora) VALUES(%s, %s)', (nombre, hora))
        mysql.connection.commit()
        flash('palabra agregada satisfactoriamente!')
        return redirect(url_for('Index'))

@app.route('/edit/<id>')
def edit(id):
    cur = mysql.connection.cursor()
    cur.execute('SELECT * FROM consulta WHERE id = %s', (id))
    data = cur.fetchall()
    return render_template('edit_consulta.html', cons = data[0])
@app.route('/update/<id>', methods=['POST'])
def update(id):
    if request.method == 'POST':
        nombre = request.form['nombre']
        hora = request.form['hora']
        cur = mysql.connection.cursor()
        cur.execute("""
        UPDATE consulta
        SET nombre = %s,
        hora = %s
        WHERE id = %s

        """, (nombre,hora, id))
        mysql.connection.commit()
        flash('consulta actualizada satisfactoriamente!')
        return redirect(url_for('Index'))

@app.route('/delete_consulta/<string:id>')
def delete_palabra(id):
    cur = mysql.connection.cursor()
    cur.execute('DELETE FROM consulta WHERE id = {0}'.format(id))
    mysql.connection.commit()
    flash('consulta borrada satisfactoriamente!')
    return redirect(url_for('Index'))



if __name__ == '__main__':
    app.run(port= 3000, debug= True)